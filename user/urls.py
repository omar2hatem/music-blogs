from django.urls import path

from .views import (
    UserDetailView,
    UserRedirectView,
    UserUpdateView, LogoutView, LoginView,
)

app_name = "user"

urlpatterns = [
    path("redirect/", view=UserRedirectView.as_view(), name="redirect"),
    path("update/", view=UserUpdateView.as_view(), name="update"),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path("user/<str:username>/", view=UserDetailView.as_view(), name="detail"),

]
