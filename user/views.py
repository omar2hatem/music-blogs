from django.contrib import messages, auth
from django.contrib.auth import get_user_model, authenticate
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.views.generic import DetailView, RedirectView, UpdateView
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import AllowAny
from rest_framework.renderers import TemplateHTMLRenderer, JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView

from user.api.serializers import LoginSerializer

User = get_user_model()


class UserDetailView(LoginRequiredMixin, DetailView):

    model = User
    slug_field = "username"
    slug_url_kwarg = "username"


class UserUpdateView(LoginRequiredMixin, UpdateView):

    model = User
    fields = ["name"]

    def get_success_url(self):
        return reverse("user:detail", kwargs={"username": self.request.user.username})

    def get_object(self):
        return User.objects.get(username=self.request.user.username)

    def form_valid(self, form):
        messages.add_message(
            self.request, messages.INFO, _("Info successfully updated")
        )
        return super().form_valid(form)


class UserRedirectView(LoginRequiredMixin, RedirectView):

    permanent = False

    def get_redirect_url(self):
        return reverse("user:detail", kwargs={"username": self.request.user.username})


class LoginView(CreateAPIView):
    """
    Login view depends on django user model
    """
    renderer_classes = [TemplateHTMLRenderer, JSONRenderer]
    serializer_class = LoginSerializer
    # permission_classes = (IsAuthenticated,)
    permission_classes = (AllowAny,)

    template_name = "login.html"

    def get(self, request, *args, **kwargs):
        #print("login:",request.user)
        serializer = self.get_serializer()
        data = {"serializer": serializer}
        return Response(data)

    def post(self, request, *args, **kwargs):
        mail = request.data["username"]
        password = request.data["password"]
        user = authenticate(request, username=mail, password=password)
        request.user = user
        if user is not None:
            auth.login(request, user)
            request.session["user"] = user.username
            request.user = user
            return Response({"user": "done"})

        return Response({"user": "failed"})


class LogoutView(APIView):
    """
    Logout view that delete sessions related to login user
    """
    def get(self, request, *args, **kwargs):
        try:
            del request.session["user"]
        except KeyError:
            pass
        return redirect('user:login')

