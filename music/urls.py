from django.urls import path
from .views import MusicViewer, PostMusicViewer, MusicUploadView, CommentMusicViewer, MusicListViewer

app_name = "music"

urlpatterns = [
    path('music/<int:id>/', MusicViewer.as_view(), name='music_dt'),
    path('music/comment/', CommentMusicViewer.as_view(), name='comment'),
    path('music/new/', PostMusicViewer.as_view(), name='music_new'),
    path('mp3-upload/', MusicUploadView.as_view(), name="mp3_upload"),

]