from django.core.validators import FileExtensionValidator
from django.urls import reverse_lazy
from djvue.serializers import FileUploadSerializer
from rest_framework import serializers
from djvue.fields import FileField
from .models import MusicModel, ReactModel, MusicLover
from user.api.serializers import UserSerializer
from user.models import User


class MusicSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=200, required=True)
    description = serializers.CharField(max_length=1500, required=True)
    file = FileField(required=True, style={"upload_url": reverse_lazy("music:mp3_upload")}, )

    class Meta:
        model = MusicModel
        fields = {
            'title',
            'description',
            'file'
        }

    def create(self, validated_data):
        file = validated_data.pop("file", None)
        music_model = MusicModel(**validated_data)
        music_model.save()
        if file is not None and all(
                [file.get("path", False), file.get("filename", False)]
        ):
            with open(file["path"], "rb") as f:
                music_model.file.save(file["filename"], f)
        return music_model


class MusicLoverSerializer(serializers.Serializer):
    user = UserSerializer()
    display_name = serializers.CharField(required=True, max_length=200)


class ReactSerializer(serializers.Serializer):
    comment = serializers.CharField(required=False, max_length=1200)
    owner = serializers.IntegerField(required=True, )
    music = serializers.IntegerField(required=True, )

    class Meta:
        model = ReactModel
        fields = {
            'comment',
            'owner',
            'music'
        }

    def create(self, validated_data):
        react = ReactModel()
        react.comment = validated_data["comment"]
        user = User.objects.get(id=validated_data["owner"])
        try:
            music_lover = MusicLover.objects.get(user_id=user.id)
        except Exception:
            music_lover = MusicLover()
            music_lover.user = user
            music_lover.display_name = user.username
            music_lover.save()

        react.owner = music_lover
        react.music = MusicModel.objects.get(id=validated_data["music"])
        react.save()
        return react


class MusicUploadSerializer(FileUploadSerializer):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["file"].validators.append(FileExtensionValidator(allowed_extensions=['mp3']))
