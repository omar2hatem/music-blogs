from django.db import models


from user.models import User


class MusicModel(models.Model):
    title = models.CharField(max_length=200, unique=True, help_text="Unique Title of the music")
    file = models.FileField(upload_to="music/", help_text="Music's file")
    description = models.CharField(max_length=1500)


class MusicLover(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    display_name = models.CharField(max_length=100)


class ReactModel(models.Model):
    music = models.ForeignKey(MusicModel, on_delete=models.CASCADE, default=1)
    comment = models.CharField(null=True, max_length=1100)
    owner = models.ForeignKey(MusicLover, on_delete=models.CASCADE)
