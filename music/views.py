from django.shortcuts import redirect
from djvue.views import FileUploadView
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import AllowAny
from rest_framework.renderers import TemplateHTMLRenderer, JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import MusicModel, ReactModel
from .serializers import MusicUploadSerializer, MusicSerializer, ReactSerializer
from user.models import User


class MusicListViewer(APIView):
    """
    A simple view to show music list
    """
    renderer_classes = [TemplateHTMLRenderer, JSONRenderer]
    template_name = 'musiclist.html'

    def get(self, request, *args, **kwargs):
        try:
            user = request.session["user"]
        except KeyError:
            return redirect('user:login')

        musics = MusicModel.objects.all()
        data = {"musics": musics}
        return Response(data)


class MusicUploadView(FileUploadView):
    """
    Helper view to upload mp3 music file
    """
    permission_classes = (AllowAny,)
    serializer_class = MusicUploadSerializer


class MusicViewer(APIView):
    """
    Main view to show the music with comments functionality
    """
    renderer_classes = [TemplateHTMLRenderer, JSONRenderer]
    permission_classes = (AllowAny,)
    template_name = "music.html"

    def get(self, request, *args, **kwargs, ):
        try:
            user = request.session["user"]
        except KeyError:
            return redirect('user:login')

        serializer = ReactSerializer
        id = self.kwargs['id']
        music = MusicModel.objects.get(id=id)
        comments = ReactModel.objects.all().filter(music=id)
        user = User.objects.get(username=request.session["user"])

        data = {
            "music": music,
            "serializer": serializer,
            "comments": comments,
            "user": user
        }
        #print(data)
        return Response(data)


class CommentMusicViewer(CreateAPIView):
    """
    API view for commenting on the music
    """
    renderer_classes = [TemplateHTMLRenderer, JSONRenderer]
    serializer_class = ReactSerializer

    def post(self, request, *args, **kwargs):
        try:
            user = request.session["user"]
        except KeyError:
            return redirect('user:login')

        serializer = ReactSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({"msg": "serializer.data"})


class PostMusicViewer(CreateAPIView):
    """
    Create Music view API
    """
    renderer_classes = [TemplateHTMLRenderer, JSONRenderer]
    serializer_class = MusicSerializer
    permission_classes = (AllowAny,)
    template_name = "new_music.html"

    def get(self, request, *args, **kwargs):
        try:
            user = request.session["user"]
        except KeyError:
            return redirect('user:login')

        serializer = self.get_serializer()
        data = {
            "serializer": serializer
        }
        return Response(data)
