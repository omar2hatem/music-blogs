===============================
Music Blogs
===============================

.. image:: https://badge.fury.io/py/second.png
    :target: http://badge.fury.io/py/second

.. image:: https://pypip.in/d/second/badge.png
    :target: https://crate.io/packages/second?version=latest


Music Blogs website

* Free software: BSD license

Requirements
------------

* Django 2.2+
* Python 3.6+

Installation
----------------------------

#. Clone the git repository.
#. Please check `Settings`_ section for configurations

#. Install all third party packages by running::

    $ pip install -r requirements.txt

#. Apply migrations::

    $ python manage.py migrate


Settings
========

This project relies extensively on environment settings.

For configuration purposes, the following table maps environment variables to their Django setting and project settings:
