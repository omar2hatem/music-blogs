from django.contrib import messages
from django.contrib.auth import get_user_model, authenticate, login, logout
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.views.generic import DetailView, RedirectView, UpdateView
from django.views.generic.edit import FormView
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import AllowAny
from rest_framework.renderers import TemplateHTMLRenderer, JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView

from user.api.serializers import LoginSerializer

from music.views import CsrfExemptSessionAuthentication

from .api.serializers import UserSerializer, SignupSerializer

User = get_user_model()


class UserDetailView(LoginRequiredMixin, DetailView):
    model = User
    slug_field = "username"
    slug_url_kwarg = "username"


class UserUpdateView(LoginRequiredMixin, UpdateView):
    model = User
    fields = ["name"]

    def get_success_url(self):
        return reverse("user:detail", kwargs={"username": self.request.user.username})

    def get_object(self):
        return User.objects.get(username=self.request.user.username)

    def form_valid(self, form):
        messages.add_message(
            self.request, messages.INFO, _("Info successfully updated")
        )
        return super().form_valid(form)


class UserRedirectView(LoginRequiredMixin, RedirectView):
    permanent = False

    def get_redirect_url(self):
        return reverse("user:detail", kwargs={"username": self.request.user.username})


class UserLoginView(FormView):
    template_name = "login.html"
    form_class = AuthenticationForm
    success_url = '/musiclist/'

    def form_valid(self, form):
        login(self.request, form.get_user())
        return super(UserLoginView, self).form_valid(form)


class SignupView(CreateAPIView):
    renderer_classes = [TemplateHTMLRenderer, JSONRenderer]
    serializer_class = SignupSerializer
    permission_classes = [AllowAny]
    template_name = "signup.html"
    queryset = User.objects.all()

    def get(self, request, *args, **kwargs):
        # print("login:",request.user)
        serializer = self.get_serializer()
        data = {"serializer": serializer}
        return Response(data)

    def post(self, request, *args, **kwargs):
        serializer = SignupSerializer(data=request.data)
        serializer.is_valid()
        serializer.save()
        return Response({"user": "done"})


class LoginView(CreateAPIView):
    """
    Login view depends on django user model
    """
    renderer_classes = [TemplateHTMLRenderer, JSONRenderer]
    serializer_class = LoginSerializer
    # permission_classes = (IsAuthenticated,)
    permission_classes = (AllowAny,)
    authentication_classes = [CsrfExemptSessionAuthentication]
    template_name = "login.html"

    def get(self, request, *args, **kwargs):
        # print("login:",request.user)
        serializer = self.get_serializer()
        data = {"serializer": serializer}
        if request.user.is_authenticated:
            return redirect('musiclist')
        return Response(data)

    def post(self, request, *args, **kwargs):
        mail = request.data["username"]
        password = request.data["password"]
        user = authenticate(request, username=mail, password=password)
        if user is not None:
            login(request, user)
            return Response({"user": "done", "token": user.auth_token.key})

        return Response({"failed": "Login Failed!"})


class LogoutView(APIView):
    """
    Logout view that delete sessions related to login user
    """
    authentication_classes = []
    permission_classes = [AllowAny,]

    def get(self, request, *args, **kwargs):
        try:
            logout(request)
        except KeyError:
            pass
        return redirect('user:login')
