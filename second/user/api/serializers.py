from django.contrib.auth import get_user_model
from rest_framework import serializers
from rest_framework.authtoken.models import Token

from music.models import MusicLover

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["username", "email", "url"]

        extra_kwargs = {
            "url": {"view_name": "user:detail", "lookup_field": "username"}
        }


class SignupSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=100, required=True)
    password = serializers.CharField(max_length=100, required=True, style={"input_type": "password"})
    confirm_password = serializers.CharField(write_only=True,max_length=100, style={"input_type": "password"})
    email = serializers.CharField(write_only=True, )

    def create(self, validated_data):
        user = User(
            email=validated_data['email'],
            username=validated_data['username']
        )
        if validated_data['password'] != validated_data['confirm_password']:
            raise KeyError
        user.set_password(validated_data['password'])
        user.save()
        musiclover = MusicLover(user=user,display_name=user.username)
        musiclover.save()
        Token.objects.create(user=user)
        return user



class LoginSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=100, required=True)
    password = serializers.CharField(write_only=True, style={"input_type": "password"})

    def create(self, validated_data):
        return validated_data
