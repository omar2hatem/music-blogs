from django.urls import path
from django.contrib.auth import views as auth_views
from .views import (
    UserDetailView,
    UserRedirectView,
    UserUpdateView, LogoutView, LoginView, UserLoginView, SignupView,
)

app_name = "user"

urlpatterns = [
    path("redirect/", view=UserRedirectView.as_view(), name="redirect"),
    path("update/", view=UserUpdateView.as_view(), name="update"),
    path('login/', LoginView.as_view(), name='login'),
    path('signup/', SignupView.as_view(), name='signup'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path("user/<str:username>/", view=UserDetailView.as_view(), name="detail"),

]
