from django.contrib.auth.models import User
from django.contrib import admin
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.aggregates import Count


def validate_title(value):
    try:
        old = MusicModel.objects.get(title=value)
        raise ValidationError(
            message='Title is duplicated!'
        )
    except MusicModel.DoesNotExist:
        return None


class MusicLover(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    display_name = models.CharField(max_length=100)

    def __str__(self):
        return '%s' % self.display_name


class MusicListeningModel(models.Model):
    time = models.DateTimeField(auto_now=True)
    musiclover = models.ForeignKey(MusicLover, on_delete=models.CASCADE)
    music = models.ForeignKey('MusicModel', on_delete=models.CASCADE)

    def __str__(self):
        return '%s in %s' % (self.music, self.time)


class MusicModel(models.Model):
    title = models.CharField(max_length=200, unique=True, help_text="Unique Title of the music"
                             , validators=[validate_title])
    file = models.FileField(upload_to="music/", help_text="Music's file")
    description = models.CharField(max_length=1500)
    listeners = models.ManyToManyField(MusicLover, through=MusicListeningModel)
    created_by = models.ForeignKey(MusicLover, on_delete=models.CASCADE, related_name='created_by')

    def get_votes(self):
        votes = VoteModel.objects.filter(music=self)
        res = 0
        if votes:
            for v in votes:
                res += v.value
        return res

    def get_listen(self):
        res = MusicListeningModel.objects.filter(music=self).aggregate(listen_count=Count('*'))
        return res['listen_count']

    def get_votes_user(self, user):
        votes = VoteModel.objects.filter(music=self, owner__user=user)
        if votes is None:
            return votes
        return votes[0]

    def __str__(self):
        return '%s | %s' % (self.title, self.created_by)


class ReactModel(models.Model):
    music = models.ForeignKey(MusicModel, on_delete=models.CASCADE, default=1)
    comment = models.CharField(null=True, max_length=1100)
    owner = models.ForeignKey(MusicLover, on_delete=models.CASCADE)

    def __str__(self):
        return 'React %s on %s' % (self.owner.display_name,self.music.title)


class VoteModel(models.Model):
    value = models.IntegerField()
    owner = models.ForeignKey(MusicLover, on_delete=models.SET_NULL, null=True)
    music = models.ForeignKey(MusicModel, on_delete=models.CASCADE)

    def __str__(self):
        return 'vote in:(%s) by %s' % (self.music, self.owner)


