from django.contrib.auth.models import User
from django.core.validators import FileExtensionValidator
from django.db.models import F
from django.urls import reverse_lazy
from djvue.serializers import FileUploadSerializer
from rest_framework import serializers
from djvue.fields import FileField
from music.models import MusicModel, ReactModel, MusicLover
from rest_framework.generics import get_object_or_404
from user.api.serializers import UserSerializer
from music.models import VoteModel, MusicListeningModel, validate_title



class MusicAPISerializer(serializers.ModelSerializer):
    class Meta:
        model = MusicModel
        fields = [
            'id',
            'title',
            'description',
            'file',
            'url'
        ]
        extra_kwargs = {
            "url": {"view_name": "music_api:music", "lookup_field": "id"}
        }


class MusicSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False)
    title = serializers.CharField(max_length=200, required=True, validators=[validate_title])
    description = serializers.CharField(max_length=1500, required=True)
    file = FileField(required=True, style={"upload_url": reverse_lazy("music:mp3_upload")}, )
    created_by_id = serializers.IntegerField(required=True)

    class Meta:
        model = MusicModel
        fields = {
            'id',
            'title',
            'description',
            'file',
            'created_by',
            'created_by_id'
        }

    def create(self, validated_data):
        file = validated_data.pop("file", None)
        try:
            user = MusicLoverSerializer(validated_data['created_by'])
        except KeyError:
            user = MusicLover.objects.get(id=validated_data.pop("created_by_id", None))

        #update current music
        try:
            id = validated_data['id']
            music_model = MusicModel.objects.get(id=id)
        except KeyError:
            music_model = MusicModel()
            music_model.created_by = user

        music_model.title = validated_data['title']
        music_model.description = validated_data['description']

        if file is not None and all(
                [file.get("path", False), file.get("filename", False)]
        ):
            with open(file["path"], "rb") as f:
                music_model.file.save(file["filename"], f)
        music_model.save()
        return music_model


class MusicLoverSerializer(serializers.Serializer):
    user = UserSerializer()
    display_name = serializers.CharField(required=True, max_length=200)


class ReactAPISerializer(serializers.Serializer):
    id = serializers.IntegerField()
    comment = serializers.CharField(required=False, max_length=1200)
    owner = MusicLoverSerializer(many=False)
    music = MusicAPISerializer(many=False)

    class Meta:
        model = ReactModel
        fields = {
            'id'
            'comment',
            'owner',
            'music'
        }


class ReactSerializer(serializers.Serializer):
    comment = serializers.CharField(required=False, max_length=1200)
    owner = serializers.IntegerField(required=True, )
    music = serializers.IntegerField(required=True, )

    class Meta:
        model = ReactModel
        fields = {
            'comment',
            'owner',
            'music'
        }

    def create(self, validated_data):
        react = ReactModel()
        react.comment = validated_data["comment"]
        user = User.objects.get(id=validated_data["owner"])
        try:
            music_lover = MusicLover.objects.get(user_id=user.id)
        except Exception:
            music_lover = MusicLover()
            music_lover.user = user
            music_lover.display_name = user.username
            music_lover.save()

        react.owner = music_lover
        react.music = MusicModel.objects.get(id=validated_data["music"])
        react.save()
        return react


class ListenSerializer(serializers.Serializer):
    import datetime
    time = serializers.DateTimeField(required=False, default=datetime.datetime.now())
    owner = serializers.IntegerField(required=True, )
    music = serializers.IntegerField(required=True, )
    first = serializers.IntegerField(required=True, )

    class Meta:
        model = VoteModel
        fields = {
            'time',
            'owner',
            'music',
            'first'
        }

    def create(self, validated_data):
        listen = MusicListeningModel()
        user = get_object_or_404(User, id=validated_data["owner"])
        first = validated_data['first']
        try:
            music_lover = MusicLover.objects.get(user_id=user.id)
        except MusicLover.DoesNotExist:
            music_lover = MusicLover()
            music_lover.user = user
            music_lover.display_name = user.username
            music_lover.save()

        music = get_object_or_404(MusicModel, id=validated_data["music"])
        listen.musiclover = music_lover
        listen.music = music
        if first == 1:
            listen.save()

        return listen


class VoteSerializer(serializers.Serializer):
    value = serializers.IntegerField(required=True, )
    owner = serializers.IntegerField(required=True, )
    music = serializers.IntegerField(required=True, )

    class Meta:
        model = VoteModel
        fields = {
            'value',
            'owner',
            'music'
        }

    def create(self, validated_data):
        vote = VoteModel()
        newvalue = validated_data["value"]
        user = get_object_or_404(User, id=validated_data["owner"])
        try:
            music_lover = MusicLover.objects.get(user_id=user.id)
        except MusicLover.DoesNotExist:
            music_lover = MusicLover()
            music_lover.user = user
            music_lover.display_name = user.username
            music_lover.save()

        music = get_object_or_404(MusicModel, id=validated_data["music"])
        votes = VoteModel.objects.filter(music=music, owner=music_lover)

        if len(votes) == 1:
            vote = votes[0]
        elif len(votes) > 1:
            for v in votes[1:]:
                v.delete()
            vote = votes[0]
        else:
            vote.owner = music_lover
            vote.music = music

        vote.value = newvalue
        vote.save()
        return vote


class MusicUploadSerializer(FileUploadSerializer):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["file"].validators.append(FileExtensionValidator(allowed_extensions=['mp3']))
