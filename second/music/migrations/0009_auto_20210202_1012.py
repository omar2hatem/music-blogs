# Generated by Django 2.2.16 on 2021-02-02 07:12

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('music', '0008_auto_20210201_1516'),
    ]

    operations = [
        migrations.AlterField(
            model_name='votemodel',
            name='value',
            field=models.IntegerField(),
        ),
        migrations.CreateModel(
            name='MusicListeningModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('time', models.DateTimeField(default=datetime.datetime(2021, 2, 2, 10, 12, 13, 552667))),
                ('music', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='music.MusicModel')),
                ('musiclover', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='music.MusicLover')),
            ],
        ),
        migrations.AddField(
            model_name='musicmodel',
            name='listeners',
            field=models.ManyToManyField(through='music.MusicListeningModel', to='music.MusicLover'),
        ),
    ]
