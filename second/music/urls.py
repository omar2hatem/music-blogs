from django.urls import path
from .views import MusicViewer, PostMusicViewer, MusicUploadView, CommentMusicViewer, VoteMusicViewer, \
    ListenMusicViewer, UpdateMusicViewer, DeleteMusic, DeleteReactView

app_name = "music"

urlpatterns = [
    path('music/<int:id>/', MusicViewer.as_view(), name='music_dt'),
    path(r'music/<int:id>/<int:p>/', MusicViewer.as_view(), name='music_dt'),
    path('music/listen/', ListenMusicViewer.as_view(), name='listen'),
    path('music/vote/', VoteMusicViewer.as_view(), name='vote'),
    path('music/comment/', CommentMusicViewer.as_view(), name='comment'),
    path('music/new/', PostMusicViewer.as_view(), name='music_new'),
    path('music/update/<int:id>/', UpdateMusicViewer.as_view(), name='music_update'),
    path('music/delete/comment/', DeleteReactView.as_view(), name='react_del'),
    path('music/delete/<int:id>/', DeleteMusic.as_view(), name='music_del'),
    path('mp3-upload/', MusicUploadView.as_view(), name="mp3_upload"),

]
