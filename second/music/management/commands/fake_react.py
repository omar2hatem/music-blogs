from __future__ import unicode_literals

import random

from django.contrib.auth.models import User
from django.core.management import BaseCommand
import csv

from future.types.newstr import unicode

from music.models import MusicLover, MusicModel


class Command(BaseCommand):
    help = (
        "Generate fake Music React into csv file",
        "Expected parameters (file_path , username , music , n) "
    )

    def add_arguments(self, parser):
        parser.add_argument(
            "file_path",
            nargs=1,
            type=unicode
        )

        parser.add_argument(
            "username",
            nargs=1,
            type=unicode
        )

        parser.add_argument(
            "music",
            nargs=1,
            type=unicode
        )

        parser.add_argument(
            "n",
            nargs=1,
            type=int
        )

    def handle(self, *args, **options):
        file_path = options["file_path"][0]
        username = options["username"][0]
        music_title = options["music"][0]
        n = options["n"][0]

        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            self.stdout.write("Error: user '%s' is not found" % username)
            return

        try:
            music = MusicModel.objects.get(title=music_title)
        except MusicModel.DoesNotExist:
            self.stdout.write("Error: Music with title '%s' is not found" % music_title)
            return

        music_lover = MusicLover.objects.get(user=user)

        words = (
            'Hi',
            'it is bad!!',
            'It is amazing :)',
            'Nice song',
            'Hello Everyone',
            'Thank You Dude',
        )
        with open(file_path, 'w') as f:

            fields = ['owner', 'music', 'comment']
            writer = csv.DictWriter(f, fieldnames=fields)
            writer.writeheader()
            for i in range(n):
                writer.writerow(
                    {
                        'owner': music_lover.id,
                        'music': music.id,
                        'comment': "'%s'" % random.choice(words)
                    }
                )
        self.stdout.write("generating the csv file is finished...")
        f.close()
