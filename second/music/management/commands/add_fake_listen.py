from __future__ import unicode_literals
from django.core.management import BaseCommand
import csv
from future.types.newstr import unicode
from music.models import  MusicLover, MusicModel, MusicListeningModel


class Command(BaseCommand):
    help = (
        "Add Music listen from csv file",
        "Expected parameters (file_path) "
    )

    def add_arguments(self, parser):
        parser.add_argument(
            "file_path",
            nargs=1,
            type=unicode
        )

    def handle(self, *args, **options):

        file_path = options["file_path"][0]
        with open(file_path) as f:
            reader = csv.reader(f)
            for rownum, (ownerid, musicid) in enumerate(reader):
                if rownum == 0:
                    continue
                owner = MusicLover.objects.get(id=ownerid)
                music = MusicModel.objects.get(id=musicid)
                listen = MusicListeningModel(musiclover=owner, music=music)
                self.stdout.write("reading %d row" % rownum)
                listen.save()
        f.close()
