from django.contrib.auth.models import User
from django.test.testcases import TestCase
from music.models import MusicModel, MusicLover, VoteModel


class MusicTest(TestCase):
    def test_get_votes(self):
        music = MusicModel(title="test")
        user = User(username="test",)
        user.set_password("test")
        user.save()

        lover = MusicLover(user=user, display_name="test")
        lover.save()

        music.save()

        vote = VoteModel(owner=lover, music=music, value=1)
        vote.save()

        self.assertEqual(music.get_votes(), 1)

