from django.contrib.auth.models import User, AnonymousUser
from django.test.client import RequestFactory
from django.test.testcases import TestCase
from django.urls import reverse
from music.views import MusicViewer
from music.models import MusicModel
from music.views import VoteMusicViewer


def get_or_create_user():
    try:
        user = User.objects.get(username="Test")
    except User.DoesNotExist:
        user = User(
            username="Test1251",
        )
        user.set_password("test")
        user.save()
    return user


def get_or_create_music():
    try:
        music = MusicModel.objects.get(title="Test")
    except MusicModel.DoesNotExist:
        music = MusicModel(title="Test")
        music.save()
    return music


class TestMusicView(TestCase):

    def test_musiclist_success_url(self):
        assert reverse("musiclist") == f"/en/musiclist/"

    def test_music_view_not_found(self):
        rf = RequestFactory()
        request = rf.get("/fake-url/")

        request.user = get_or_create_user()

        # test not found music
        response = MusicViewer.as_view()(request, id=10)
        self.assertEqual(response.status_code, 404)

    def test_music_view_found(self):
        rf = RequestFactory()
        request = rf.get("/fake-url/")
        music = get_or_create_music()
        request.user = get_or_create_user()
        response = MusicViewer.as_view()(request, id=music.id)
        self.assertEqual(response.status_code, 200)

    def test_music_view_not_authenticated(self):
        rf = RequestFactory()
        request = rf.get("/fake-url/")
        request.user = AnonymousUser()
        music = get_or_create_music()
        response = MusicViewer.as_view()(request, id=music.id)
        self.assertEqual(response.status_code, 403)

    def test_music_view_authenticated(self):
        rf = RequestFactory()
        request = rf.get("/fake-url/")
        request.user = get_or_create_user()
        music = get_or_create_music()
        response = MusicViewer.as_view()(request, id=music.id)
        self.assertEqual(response.status_code, 200)


class TestMusicVoteView(TestCase):

    def test_vote_url(self):
        assert reverse("music:vote") == f"/en/music/vote/"

    def test_vote_not_found_music(self):
        rf = RequestFactory()
        user = get_or_create_user()
        request = rf.post("/fake-url/", data={"value": 1, "owner": user.id, "music": 10})
        request.user = user
        response = VoteMusicViewer.as_view()(request)
        self.assertEqual(response.status_code, 404)

    def test_vote_found_music(self):
        rf = RequestFactory()
        user = get_or_create_user()
        music = get_or_create_music()
        request = rf.post("/fake-url/", data={"value": 1, "owner": user.id, "music": music.id})
        request.user = user
        response = VoteMusicViewer.as_view()(request)
        self.assertEqual(response.status_code, 200)

    def test_vote_music_authenticated(self):
        rf = RequestFactory()
        user = get_or_create_user()
        music = get_or_create_music()
        request = rf.post("/fake-url/", data={"value": 1, "owner": user.id, "music": music.id})
        request.user = user
        response = VoteMusicViewer.as_view()(request)
        self.assertEqual(response.status_code, 200)

    def test_vote_music_not_authenticated(self):
        rf = RequestFactory()
        user = AnonymousUser()
        music = get_or_create_music()
        request = rf.post("/fake-url/", data={"value": 1, "owner": 0, "music": music.id})
        request.user = user
        response = VoteMusicViewer.as_view()(request)
        self.assertEqual(response.status_code, 404)
