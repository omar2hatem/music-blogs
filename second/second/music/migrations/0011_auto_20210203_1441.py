# Generated by Django 2.2.16 on 2021-02-03 11:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('music', '0010_auto_20210203_1439'),
    ]

    operations = [
        migrations.AlterField(
            model_name='musiclisteningmodel',
            name='time',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
