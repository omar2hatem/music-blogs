from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.db.models.aggregates import Count, Sum
from django.shortcuts import get_object_or_404, redirect
from django.views.generic.list import ListView
from djvue.views import FileUploadView
from rest_framework.exceptions import PermissionDenied as per_denied

from rest_framework.generics import CreateAPIView, UpdateAPIView, DestroyAPIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.renderers import TemplateHTMLRenderer, JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.authentication import SessionAuthentication
from .api.serializers import VoteSerializer, ListenSerializer
from .models import MusicModel, ReactModel, MusicLover
from music.api.serializers import MusicUploadSerializer, MusicSerializer, ReactSerializer


class MusicListViewer(ListView):
    """
    A simple view to show music list
    """
    model = MusicModel
    paginate_by = 2

    def get_queryset(self):
        query = self.request.GET.get('query')
        if query is None:
            return MusicModel.objects.all().annotate(views=Count('listeners')).order_by('-views')
        else:
            return MusicModel.objects.filter(title__contains=query).annotate(views=Count('listeners')).order_by(
                '-views')

    def get_context_data(self, **kwargs):
        if not self.request.user.is_authenticated:
            raise PermissionDenied

        query = self.request.GET.get('query', '')
        context = super().get_context_data(**kwargs)
        all_music = MusicModel.objects.all().exclude(votemodel=None).annotate(votes=Sum('votemodel__value')).order_by(
            '-votes')
        mymusic = MusicModel.objects.filter(created_by__user=self.request.user).annotate(
            views=Count('listeners')).order_by(
            '-views')

        context['search_query'] = query
        context['musics'] = self.object_list
        context['allmusics'] = all_music[:5]
        context['mymusic'] = mymusic
        return context


class CsrfExemptSessionAuthentication(SessionAuthentication):
    def enforce_csrf(self, request):
        return


class MusicUploadView(FileUploadView):
    """
    Helper view to upload mp3 music file
    """
    renderer_classes = [TemplateHTMLRenderer, JSONRenderer]
    permission_classes = (IsAuthenticated,)
    serializer_class = MusicUploadSerializer
    authentication_classes = [CsrfExemptSessionAuthentication]


class MusicViewer(APIView):
    """
    Main view to show the music with comments functionality
    """
    renderer_classes = [TemplateHTMLRenderer, JSONRenderer]
    permission_classes = (IsAuthenticated,)
    template_name = "music.html"

    def get(self, request, *args, **kwargs, ):
        serializer = ReactSerializer
        voteserializer = VoteSerializer
        id = self.kwargs['id']
        music = get_object_or_404(MusicModel, id=id)
        comments = ReactModel.objects.all().filter(music=id)
        user = User.objects.get(username=request.user)
        listen = music.get_listen()
        try:
            lover = MusicLover.objects.get(user=user)

        except MusicLover.DoesNotExist:
            return redirect('user:login')

        update = None
        if music.created_by == lover:
            update = 1

        data = {
            "music": music,
            "serializer": serializer,
            "comments": comments,
            "user": lover,
            "update": update,
            "listen": listen,
            "votes": music.get_votes(),
            "voteserializer": voteserializer
        }
        return Response(data)


class CommentMusicViewer(CreateAPIView):
    """
    API view for commenting on the music
    """
    renderer_classes = [TemplateHTMLRenderer, JSONRenderer]
    serializer_class = ReactSerializer
    permission_classes = (IsAuthenticated,)
    authentication_classes = [CsrfExemptSessionAuthentication]

    def post(self, request, *args, **kwargs):
        serializer = ReactSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({"msg": "serializer.data"})


class VoteMusicViewer(CreateAPIView):
    """
    API view for commenting on the music
    """
    renderer_classes = [JSONRenderer]
    serializer_class = VoteSerializer
    permission_classes = (AllowAny,)
    authentication_classes = [CsrfExemptSessionAuthentication]

    def post(self, request, *args, **kwargs):
        serializer = VoteSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({"msg": "request.data"})


class ListenMusicViewer(CreateAPIView):
    """
    API view for save listening requests on music
    """
    renderer_classes = [JSONRenderer]
    serializer_class = ListenSerializer
    permission_classes = (AllowAny,)
    authentication_classes = [CsrfExemptSessionAuthentication]

    def post(self, request, *args, **kwargs):
        serializer = ListenSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({"msg": "request.data"})


class PostMusicViewer(CreateAPIView):
    """
    Create Music view API
    """
    renderer_classes = [TemplateHTMLRenderer, JSONRenderer]
    serializer_class = MusicSerializer
    permission_classes = (IsAuthenticated,)
    template_name = "new_music.html"
    authentication_classes = [CsrfExemptSessionAuthentication]

    def get(self, request, *args, **kwargs):
        serializer = self.get_serializer()
        lover = MusicLover.objects.get(user=request.user)
        data = {
            "user": lover,
            "serializer": serializer
        }
        return Response(data)


class DeleteMusic(APIView):
    renderer_classes = [TemplateHTMLRenderer, JSONRenderer]
    permission_classes = (IsAuthenticated,)
    authentication_classes = [CsrfExemptSessionAuthentication]

    def post(self, request, id):
        music = get_object_or_404(MusicModel, id=id)
        if request.user != music.created_by.user:
            raise per_denied('You cannot delete music not yours!')
        music.delete()
        return Response("{'msg':'done'}")


class DeleteReactView(APIView):
    renderer_classes = [TemplateHTMLRenderer, JSONRenderer]
    permission_classes = (IsAuthenticated,)
    authentication_classes = [CsrfExemptSessionAuthentication]
    serializer_class = ReactSerializer

    def post(self, request):
        react = get_object_or_404(ReactModel, id=request.data['id'])
        if request.user != react.owner.user:
            raise per_denied('You cannot delete music not yours!')

        react.delete()
        return Response("{'msg':'done'}")


class UpdateMusicViewer(UpdateAPIView):
    """
    update Music view API
    """
    lookup_field = 'id'
    renderer_classes = [TemplateHTMLRenderer, JSONRenderer]
    serializer_class = MusicSerializer
    permission_classes = (IsAuthenticated,)
    template_name = "update_music.html"
    authentication_classes = [CsrfExemptSessionAuthentication]
    queryset = MusicModel.objects.all()

    def get(self, request, *args, **kwargs):
        id = kwargs['id']
        music = get_object_or_404(MusicModel, id=id)
        serializer = MusicSerializer(music)
        if request.user != music.created_by.user:
            raise per_denied('cannot edit music not yours!')
        data = {
            "sdata": serializer.data,
            "serializer": serializer
        }
        return Response(data)

    def post(self, request, id):
        serializer = MusicSerializer(data=request.data)
        v = serializer.is_valid(raise_exception=True)
        if v:
            serializer.save()
        return Response("{'msg':'done'}")
