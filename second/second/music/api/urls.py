from django.urls import path
from rest_framework.routers import SimpleRouter
from .views import MusicViewSet, ReactViewSet, MusicsViewSet, MusicsViewV2, MusicViewV2

app_name = "music_api"

router = SimpleRouter()

#router.register("musics", MusicsViewSet, basename="musics")
router.register("reacts", ReactViewSet, basename="reacts")

urlpatterns = router.urls
urlpatterns += [
    path("music/", MusicsViewV2.as_view(), name="musics"),
    path("music/<int:id>/", MusicViewV2.as_view(), name="music"),

]