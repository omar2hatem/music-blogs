from rest_framework import generics
from rest_framework.authentication import TokenAuthentication
from rest_framework.generics import get_object_or_404
from rest_framework.mixins import RetrieveModelMixin, ListModelMixin, UpdateModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import JSONRenderer, BrowsableAPIRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet, ModelViewSet
from .serializers import MusicAPISerializer, ReactAPISerializer

from music.models import ReactModel, MusicModel


class MusicsViewV2(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    renderer_classes = [BrowsableAPIRenderer, JSONRenderer]
    serializer_class = MusicAPISerializer
    queryset = MusicModel.objects.all()


class MusicViewV2(generics.RetrieveDestroyAPIView):
    renderer_classes = [BrowsableAPIRenderer, JSONRenderer]
    permission_classes = [IsAuthenticated]
    serializer_class = MusicAPISerializer
    lookup_field = "id"
    queryset = MusicModel.objects.all()


class MusicsViewSet(ModelViewSet):
    renderer_classes = [BrowsableAPIRenderer, JSONRenderer]
    serializer_class = MusicAPISerializer
    queryset = MusicModel.objects.all()
    lookup_field = "id"


class MusicViewSet(APIView):
    queryset = MusicModel.objects.all()
    lookup_field = "id"
    renderer_classes = [BrowsableAPIRenderer, JSONRenderer]
    serializer_class = MusicAPISerializer

    def get(self, request, id):
        serializer_context = {
            'request': request,
        }
        music = get_object_or_404(MusicModel, id=id)
        data = MusicAPISerializer(music, many=False, context=serializer_context).data
        return Response(data)


class ReactViewSet(RetrieveModelMixin, ListModelMixin, UpdateModelMixin, GenericViewSet):
    renderer_classes = [BrowsableAPIRenderer, JSONRenderer]
    serializer_class = ReactAPISerializer
    queryset = ReactModel.objects.all()

