from django.contrib import admin

from .models import MusicModel, ReactModel, MusicLover, VoteModel, MusicListeningModel


class ReactInline(admin.TabularInline):
    model = ReactModel
    extra = 2


class MusicAdmin(admin.ModelAdmin):
    list_display = ['title', 'created_by', 'description', 'get_votes', 'get_listen']
    list_filter = ['title']
    ordering = ['title']
    fieldsets = [
            (None, {'fields': ['title']}),
            ('Description', {'fields': ['description']}),
            ('Music File', {'fields': ['file']}),
            ('Owner', {'fields': ['created_by']}),

    ]
    inlines = [ReactInline]


class ReactAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Comment', {'fields': ['comment']}),
        ('Music Lover', {'fields': ['owner']}),
        ('Music', {'fields': ['music']}),

    ]


class ListeningAdmin(admin.ModelAdmin):
    fieldsets = [
       # ('Listening Time', {'fields': ['time']}),
        ('Music Lover', {'fields': ['musiclover']}),
        ('Music', {'fields': ['music']}),

    ]
    list_display = ['musiclover', 'time', 'music']
    list_filter = ['time']
    ordering = ['-time']
    list_per_page = 10


class VoteAdmin(admin.ModelAdmin):
    list_display = ['owner', 'value', 'music']
    ordering = ['value']
    list_filter = ['music', 'value', 'owner']
    list_per_page = 10


admin.site.register(MusicModel, MusicAdmin)
admin.site.register(ReactModel, ReactAdmin)
admin.site.register(VoteModel, VoteAdmin)
admin.site.register(MusicListeningModel, ListeningAdmin)
admin.site.register(MusicLover)
