from .base import *
import pymysql

pymysql.install_as_MySQLdb()

"""DATABASES = {
    "default": {
        "ENGINE": os.environ.get("SQL_ENGINE", "django.db.backends.mysql"),
        "NAME": os.environ.get("SQL_DATABASE", "hello_django"),
        "USER": os.environ.get("SQL_USER", "hello_django"),
        "PASSWORD": os.environ.get("SQL_PASSWORD", "hello_django"),
        "HOST": "db",
        "PORT": os.environ.get("SQL_PORT", ""),
    },
}"""
SITE_ID = 1

DATABASES = {
    'default': env.db("DATABASE_URL", default="mysql://hello_django:hello_django@db:3306/hello_django")
}

ALLOWED_HOSTS = ["*"]

